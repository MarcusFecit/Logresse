![GitHub Logo](/PNG/Logresse-Logo-With-Text.png)

***

Logresse is a 3D Printer made out of laser cut steel sheet, derivated from Irobri's P3-Steel (http://www.reprap.org/wiki/P3Steel)

You can figure out what it looks like with the OpenSCAD 3D preview, file Logresse.scad located in the SCAD/ directory.

License CC BY-NC 3.0 (see http://creativecommons.org/licenses/by-nc/3.0/)

***

**Information**

* Français : https://www.logre.eu/wiki/Logresse
* English  : https://www.logre.eu/wiki/Logresse/en
* Espanol  : https://www.logre.eu/wiki/Logresse/es
* Italiano : https://www.logre.eu/wiki/Logresse/it
* Deutsch  : https://www.logre.eu/wiki/Logresse/de
