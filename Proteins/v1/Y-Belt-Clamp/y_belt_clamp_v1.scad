// robust belt clamp  for P3 Steel
// 04/05/2014

$fs=0.1;

// Belt characteristics for clamp
belt_height = 17; 
belt_width=7;
belt_thickness=2;
tooth_height=1;
tooth_offset =2;  // 2 for GT2, 5 for XL/T5
ram_height=3;
belt_clamp_hole_separation=24;
layer_thickness = 0.33; // print layer thickness, 0.33 should be OK for 0.25 as well

belt_clamp_width = 12;
belt_clamp_length = belt_clamp_hole_separation + belt_clamp_width + m3_diameter;

ClampWidth = belt_clamp_hole_separation + belt_clamp_width;

basex = ClampWidth + 24;
basey = belt_clamp_width + 22 ;
basez_down = 4;
basez_up = 4;
m3_nut_diameter = 5.5; 
m3_diameter = 3.6;
m3_nut_height = 3.91;

y_belt();

module y_belt() {
	// Low
	translate([basex/2,5,0])
	difference() {
		structure(basez_down);
		holes(basez_down);
	}
	// Hight
	translate([-basex/2,5,0])
	difference() {
		structure(basez_up);
		holes(basez_up);
	}
	//
	translate([-basex/2,-basey-5,0])
	difference() {
		structure(belt_height-basez_down);
		holes_m(belt_height-basez_down);
	}

	// the ram
	translate([10 , -10, 0])
		belt_ram();
}

module structure(structure_width){
	translate([0, belt_clamp_width/2, 0])  belt_clamp_solids(height=structure_width);
	translate([0, basey - belt_clamp_width/2, 0]) belt_clamp_solids(height=structure_width);
	// bridge the two clamps
	for (x=[0,1])
		translate([-ClampWidth/2 + x*(ClampWidth-belt_width), belt_clamp_width/2, 0])
			cube([belt_width, basey - belt_clamp_width, structure_width]);
}


module holes_m(holes_width) {
	translate([0, basey - belt_clamp_width/2, 0])
		belt_clamp_holes(height=holes_width, nut_holes=0, teeth=0);
	translate([0, belt_clamp_width/2, 0])
		belt_clamp_holes(height=holes_width, nut_holes=nut_holes, belt_channel=1, tightener=1, teeth=teeth, bottom_channel=bottom_channel);


}

module holes(holes_width) {

	translate([0, belt_clamp_width/2, 0])
		belt_clamp_holes(height=holes_width, nut_holes=0, teeth=1);
	translate([0, basey - belt_clamp_width/2, 0])
		belt_clamp_holes(height=holes_width, nut_holes=0);
}


module belt_clamp_solids(height=5) {
	translate([0, 0, height / 2])
		hull() {
			for(i = [-1, 1])
				translate([i * belt_clamp_hole_separation / 2, 0, 0])
					cylinder(r = belt_clamp_width / 2, h = height, center = true);
		}
}

module belt_clamp_holes(height=5, nut_holes=0, belt_channel=0, tightener=0, teeth=0, bottom_channel=0)
{
	// holes
	// screw and nut holes
	for(i=[-1, 1]) {
		// make nut holes if requested
		if (nut_holes > 0) {
			translate([i * belt_clamp_hole_separation / 2, 0, -.1])
				rotate(360 / 12)
					cylinder(r = m3_nut_diameter / 2, h = m3_nut_height+.1, $fn = 6);

			// move this up a bit to make a bridging layer
			translate([i * belt_clamp_hole_separation / 2, 0, m3_nut_height+layer_thickness])
				rotate(360 / 16)
					cylinder(r = m3_diameter / 2, h = height-m3_nut_height, $fn = 16);
		} else {
			translate([i * belt_clamp_hole_separation / 2, 0, -.05])
				rotate(360 / 16)
					cylinder(r = m3_diameter / 2, h = height+.1, $fn = 16);
		}
	}
	// belt channel if requested
	if (belt_channel > 0) {
		translate([0,0,height - belt_thickness/2+.1])
		cube([belt_width, belt_clamp_width+.2, belt_thickness], center=true);
	}
	// belt channel if requested
	if (bottom_channel > 0) {
		cube([belt_width, belt_clamp_width, belt_thickness], center=true);
	}
	// teeth if requested
	if (teeth > 0) {
		translate([0, -belt_clamp_width+belt_clamp_width, (height - (belt_thickness/4)/2)])
			cube([belt_width, belt_clamp_width, belt_thickness/4+.1], center=true);
		for(i=[0:belt_clamp_width/tooth_offset]) {
			translate([0, (-belt_clamp_width / 2) + (tooth_offset * i), (height - tooth_height/2)-belt_thickness/4])
				cube([belt_width, tooth_offset/2, tooth_height+.1], center=true);
		}
	}
	// tightener if requested
	if (tightener > 0) {
		translate([0,belt_clamp_width/2+0.05, (height-belt_thickness)/2])
			rotate([90,0,0])
				union() {
					cylinder(r=m3_nut_diameter/2, h=m3_nut_height+0.1, $fn=6);
					cylinder(r=m3_diameter/2, h=belt_clamp_width+0.1, $fn=12);
					cube([belt_width+.3, belt_width+.4,5], center=true);
				}
	}
}

module belt_ram() {
	difference() {
		union(){
			 translate([0, -belt_width / 2, 0])
				cube([belt_width, belt_width, ram_height]);
			translate([0, 0, ram_height])
				rotate([0, 90, 0])
					cylinder(r = belt_width / 2, h = belt_width, $fn = 20);
		}
		translate([belt_width / 2, 0, -.1])
			cylinder(r = m3_diameter / 2, h = belt_width/2, $fn = 8);
		translate([belt_width / 2,0, -belt_width/2])
				cube([belt_width+.2, belt_width+.2, belt_width], center=true);
	}
}
