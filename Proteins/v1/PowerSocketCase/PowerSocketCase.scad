// Marc BERLIOUX 2014
// license : "Make it Easy"

totalHeight=100;
thickness=3;
screw=4.5;

rotate([180,0,0])
translate([0,0,-totalHeight/2])
union(){

difference(){

// body
linear_extrude(height=totalHeight, center = true, convexity=5) import("PowerSocketCase.dxf",layer="profile", $fn=64);

// inside body
translate([0,0,-thickness/2])
linear_extrude(height=totalHeight-thickness, center = true, convexity=5) import("PowerSocketCase.dxf",layer="profileInt", $fn=64);

// inside body 2
translate([0,0,-12])
linear_extrude(height=totalHeight, center = true, convexity=5) import("PowerSocketCase.dxf",layer="profileInt2", $fn=64);

// socket hole
translate([0,0,0])
linear_extrude(height=totalHeight, center = true, convexity=5) import("PowerSocketCase.dxf",layer="socket", $fn=64);

// socket nuts traps
translate([20,0,totalHeight/2-9])
cylinder(r=3.4,h=12,center=true,$fn=6);
translate([-20,0,totalHeight/2-9])
cylinder(r=3.4,h=12,center=true,$fn=6);

// fixations screws
translate([0,25,38])
rotate([90,0,0])
cylinder(r=screw/2,h=20,center=true,$fn=32);
translate([0,25,-5])
rotate([90,0,0])
cylinder(r=screw/2,h=20,center=true,$fn=32);

// fixation nuts traps
translate([0,22,38])
rotate([90,90,0])
cylinder(r=4.2,h=8,center=true,$fn=6);
translate([0,22,-5])
rotate([90,90,0])
cylinder(r=4.2,h=8,center=true,$fn=6);

// wiring access
translate([2,15,-33])
cube([60,30,35],center=true);

}
}