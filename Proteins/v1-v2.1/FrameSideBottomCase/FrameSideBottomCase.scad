// Marc BERLIOUX 2014
// license : "Make it Easy"

totalHeight=30;

translate([-35,62,0])
difference(){
union(){
difference(){
// exterieur
translate([0,0,totalHeight/2])
linear_extrude(height=totalHeight, center = true, convexity=5) import("FrameSideBottomCase.dxf", layer="dessus", $fn=64);

// interieur
translate([0,0,totalHeight/2+1.5])
linear_extrude(height=totalHeight, center = true, convexity=5) import("FrameSideBottomCase.dxf", layer="dessusInterieur", $fn=64);

// trou vis avant
translate([5.2,-1,17])
rotate([0,90,90])
#cylinder(r=3.5,h=3,center=true,$fn=32);

// dégagement vis avant coté
translate([8.7,2,17])
rotate([0,0,0])
cube([8,8,7],center=true);

// dégagement vis avant haut
translate([6.7,2,27])
rotate([0,0,0])
cube([8,8,20],center=true);

// arrondi avant
translate([26,-1,30])
rotate([0,90,90])
cylinder(r=22,h=3,center=true,$fn=32);

// arrondi arrière
translate([72,-100,30])
rotate([0,90,0])
cylinder(r=22,h=3,center=true,$fn=32);

// dégagement vis arrière haut
translate([75,-117,21])
rotate([0,0,0])
cube([8,12,20],center=true);

}

translate([72,-103,4])
rotate([0,90,0])
cylinder(r=4,h=8,center=true,$fn=32);

translate([72,-95,4])
rotate([0,90,0])
cylinder(r=4,h=8,center=true,$fn=32);

translate([72,-99,4])
rotate([0,0,0])
cube([8,8,8],center=true);

translate([26,-3,4])
rotate([90,0,0])
cylinder(r=4,h=8,center=true,$fn=32);

}

// trous vis
translate([72,-103,4])
rotate([0,90,0])
cylinder(r=1,h=9,center=true,$fn=32);

translate([72,-95,4])
rotate([0,90,0])
cylinder(r=1,h=9,center=true,$fn=32);

translate([26,-3,4])
rotate([90,0,0])
cylinder(r=1,h=10,center=true,$fn=32);

translate([34,-3,4])
rotate([90,0,0])
cylinder(r=1,h=9,center=true,$fn=32);

}

