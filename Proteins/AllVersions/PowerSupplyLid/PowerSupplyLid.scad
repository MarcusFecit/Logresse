// Marc BERLIOUX 2014
// license : "Make it Easy"

totalHeight=50;

//top

translate([0,0,50])
rotate([180,0,0])

difference(){

// exterieur
difference(){

translate([-5,0,totalHeight/2])
linear_extrude(height=totalHeight, center = true, convexity=5) import("PowerSupplyLid.dxf", layer="face", $fn=64);

translate([0,0,150])
rotate([0,90,0])
linear_extrude(height=120, center = true, convexity=5) import("PowerSupplyLid.dxf", layer="profil", $fn=64);

}

//interieur
difference(){
translate([-5,0,totalHeight/2])
linear_extrude(height=totalHeight, center = true, convexity=5) import("PowerSupplyLid.dxf", layer="faceInterieur", $fn=64);

translate([0,0,150])
rotate([0,90,0])
linear_extrude(height=120, center = true, convexity=5) import("PowerSupplyLid.dxf", layer="profilInterieur", $fn=64);
}

// ergots +fils
translate([-5,0,70])
rotate([90,0,0])
linear_extrude(height=totalHeight, center = true, convexity=5) import("PowerSupplyLid.dxf", layer="dessous", $fn=64);

}