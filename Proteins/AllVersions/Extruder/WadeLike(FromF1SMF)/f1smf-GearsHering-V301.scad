// 
//pi=3.1415926535897932384626433832795;
$fs=.1;
//$fn=50;
// Paramètres liés  rayon:
// Cette section permet de définir le style de rayon de l'interrieur du pignon.
// Certaines des propriétés ne sont applicable qua certains types de rayons
// Pour créer une large gamme de modèles de d'interrieur de dessin dans le pignon .
// La "proportion" propriété affecte la façon dont certains rayons sont rendus. Le premier numéro est le
// Proportion de la conception du centre de la roue à l'intérieur de la jante, et le second
// Nombre est la proportion de la largeur intérieure de la roue. Par exemple, pour créer des rayons qui sont
// À peu près la forme d'un "U", vous pouvez utiliser un style "cercle", et définir la proportion de [1,5, 1,0],
// Pour rayons cirle qui sont 150% aussi longtemps que la distance du centre à l'intérieur de la jante,
// 100% aussi large.
//      Les styles de rayons sont :
// * Biohazard - Une conception de logo d'inspiration biohazard . Réglez numberOfSpokes à 3 pour imiter le logo .
// * circle - Bâtons dans une forme circlar ou ovale , défini par spokeWidth et proportion .
// * Circlefit - Le nombre maximal de cercles qui correspondent entre le centre et la jante ,
// Avec un ensemble de plus petits cercles extérieurs spécifiés par outerHoleDiameter .
// * diamond - Spokes dans la forme d'un diamant ( losange ) , défini par spokeWidth et proportion .
// * fill - Remplit la zone de rayon d'un cylindre solide .
// * line - rayons de droite , comme vous verriez sur une roue de wagon typique.
// * rectangle - Rayons dans la forme d'un rectangle , défini par spokeWidth et proportion .
// * spiral -  rayons en forme de demi-cercle , définies par courbure , inverse , spokeWidth .



//spokeStyle        	= "circle";   	// none, fill, biohazard, circle, circlefit, diamond, line, rectangle, spiral,teardrop
spokeStyle        	= "circlefit";   	// none, fill, biohazard, circle, circlefit, diamond, line, rectangle, spiral,teardrop
//spokeStyle        	= "diamond";   	// none, fill, biohazard, circle, circlefit, diamond, line, rectangle, spiral,teardrop
//spokeStyle        	= "biohazard";   	// none, fill, biohazard, circle, circlefit, diamond, line, rectangle, spiral,teardrop
//spokeStyle        	= "fill";   	// none, fill, biohazard, circle, circlefit, diamond, line, rectangle, spiral,teardrop
//spokeStyle        	= "line";   	// none, fill, biohazard, circle, circlefit, diamond, line, rectangle, spiral,teardrop
//spokeStyle        	= "rectangle";   	// none, fill, biohazard, circle, circlefit, diamond, line, rectangle, spiral,teardrop
//spokeStyle        	= "spiral";   	// none, fill, biohazard, circle, circlefit, diamond, line, rectangle, spiral,teardrop
//spokeStyle        	= "teardrop";   	// none, fill, biohazard, circle, circlefit, diamond, line, rectangle, spiral,teardrop
//spokeStyle        	= "none";   	// none, fill, biohazard, circle, circlefit, diamond, line, rectangle, spiral,teardrop


numberOfSpokes    = 5;          	// Number of "spokes." Set this to three if you're doing the biohazard design
spokeWidth        	= 4;          		// This is how wide each spoke is.
proportion        	= [1.5,1];  			// proportion to rim, proportion of width
curvature         	= 0.90;       		// For "spiral", this is how curvey the spokes are. >0, but <=1, where 
                                			//     1 is a half circle
reverse			= true;      		// For "spiral", setting this to "true" reverses the direction of the spirals
outerHoleDiameter = 0;         	// Pour  "circlefit", le diamètre des trous extérieurs, ou zéro pour aucun
concavity		= [0,0];      		// Concavity distance of spoke area for [inside, outside] of wheel

additional_tooth_width = 0.3; //mm
additional_tooth_depth = 0.2; //mm


m3_nut_flats = 5.70; // normal M3 hex nut exact width = 5.5
m3_nut_depth = 2.6; // normal M3 hex nut exact depth = 2.4, nyloc = 4
m3_nut_diameter=5.7;// normal M3 hex nut exact width = 5.5

m8_clearance_rad=4.6; 
m8_diameter = 9;
m8_nut_diameter = 15.5;
// Bearing 
688zz_diameter=16;
hole_for_688zz=688zz_diameter+.2;
688zz_height=5;

// Functions

function tooth_spacing(tooth_pitch,pitch_line_offset,teeth)
	= (2*((teeth*tooth_pitch)/(3.14159265*2)-pitch_line_offset)) ;

echo(version=version());
WadesL(); 
//WadesS(); 
//WadeL_double_helix();
//rotate([0,180,0]) WadesS_double_helix();
//WadebeltL();
//WadebeltS();
//bearing_guide();
//test();


module bearing_guide(){
	teeth=38;
	rim_thickness=7;
	hub_diameter=0;
	hub_thickness=0;
	idler = 1;
	idler_ht = 1;
	retainer = 1;
	retainer_ht = 1;
	gear_thickness=rim_thickness+2;
	ring_thickness = rim_thickness+idler_ht+retainer_ht-688zz_height;
	dist_guide = 25;
	difference(){
		pulley (
			teeth, 
			retainer=retainer, 
			retainer_ht=retainer_ht,
			idler=idler, 
			idler_ht=idler_ht,
			gear_thickness=gear_thickness,
			rim_thickness=rim_thickness,
			rim_width = rim_thickness,
			hub_thickness =hub_thickness,
			hub_diameter=hub_diameter,
			bore_diameter=0,
			spokeStyle="fill",
			tooth_depth=0.764,
			tooth_width =1.494 );
	if (! idler) assign(idler_ht=0);
	if (! retainer) assign(retainer_ht=0);
	translate([0,0,ring_thickness/2]) 
			cylinder(r=hole_for_688zz/2,h=rim_thickness+idler_ht+retainer_ht,$fn=50);
	translate([0,0,-.1]) 
			cylinder(r=(hole_for_688zz-2)/2,h=rim_thickness);
	translate([0,0,rim_thickness+idler_ht+retainer_ht-(ring_thickness/2)]) 
			cylinder(r=(hole_for_688zz+2)/2,h=ring_thickness,$fn=50);

	}
	translate([dist_guide,0,0]) {
		difference(){		
			translate([0,0,0]) 
				cylinder(r=(hole_for_688zz+2)/2-.2,h=ring_thickness/2,$fn=50);
			translate([0,0,0]) 
				difference() {
					translate([0,0,-.1]) 					
						cylinder(r=(hole_for_688zz+2)/2+.1,h=.4);
					translate([0,0,-.2]) 
						cylinder(r2=(hole_for_688zz+2)/2-.2+.1,r1=(hole_for_688zz+1)/2,h=.6);
				}
			translate([0,0,-.1]) 
				cylinder(r=(hole_for_688zz-2)/2,h=rim_thickness+idler_ht+retainer_ht,$fn=50);
		}
	}
}
module WadebeltL(){
	teeth=88;
	height=10;
	hub_diameter=22;
	hub_thickness=10;
	gear_thickness=4.5;
	difference(){
		pulley (
			number_of_teeth=teeth, 
			retainer=1, 
			retainer_ht=1,
			idler=1, 
			idler_ht=1,
			gear_thickness=gear_thickness,
			rim_thickness=7.1,
			rim_width = 4.5,
			hub_thickness =hub_thickness,
			hub_diameter=hub_diameter,
			bore_diameter=8.3,
			spokeStyle=spokeStyle,
			tooth_depth=0.764,
			tooth_width =1.494 );

		translate([0,0,gear_thickness]) cylinder(r=m8_nut_diameter/2,h=hub_thickness,$fn=6);

	}
}



module WadeL_double_helix(){
	//Large WADE's Gear - Double Helix
	teeth=47;
	twist=200;
	height=10;
	
	pressure_angle=30;
	difference(){
		union(){
		gear (number_of_teeth=teeth,
			circular_pitch=268,
			pressure_angle=pressure_angle,
			clearance = 0.2,
			gear_thickness =0,// height/2*0.5,
			rim_thickness = height/2,
			rim_width = 5,
			hub_thickness = height/2*1.5-2,
			hub_diameter = 22,
			bore_diameter = 8.3,
			spokeStyle="none",
			chamfer=1,
			twist = twist/teeth); 
		mirror([0,0,1])
		gear (number_of_teeth=teeth,
			circular_pitch=268,
			pressure_angle=pressure_angle,
			clearance = 0.2,
			gear_thickness = height/2,
			rim_thickness = height/2,
			rim_width = 5,
			hub_thickness = height/2,
			hub_diameter=22,
			bore_diameter=8.3,
			spokeStyle=spokeStyle,
			chamfer=1,
			twist=twist/teeth);
		}
		translate([0,0,height/2]) cylinder($fn=6,r=m8_nut_diameter/2,h=height-4.9);

		translate([0,0,1])rotate([180,0,0])m8_hole_vert_with_hex(100);

		translate([0,0,4.3]) difference(){ // reduce width and smooth
			cylinder(r=25.1/2, h=2);
			cylinder(r1=25.1/2,r2=20.1/2,h=4);
		}
	}
}

module WadesL(){
	// Large WADE's Gear
	echo("module WadesL");
	number_of_teeth=43; //43
	hub_diameter=22;
	hub_thickness=10;
	gear_thickness=5;
	difference(){
		gear (number_of_teeth,
			circular_pitch=268,
			gear_thickness= gear_thickness,
			rim_thickness= 10,
			rim_width = 4,
			hub_thickness=hub_thickness,
			hub_diameter= hub_diameter,
			bore_diameter= 8.3,
			spokeStyle=spokeStyle,
			chamfer=1,
			twist = 0);

		translate([0,0,gear_thickness]) cylinder(r=m8_nut_diameter/2,h=hub_thickness-4.9,$fn=6);
	}
}
module WadebeltS(){
	//small WADE's Gear
	echo("module WadesS");
	number_of_teeth=20;
	hub_diameter= 20;
	rim_thickness=7.1;
	hub_thickness =18;

	idler_ht = 1;
	retainer_ht = 1;
	difference() {
		translate([0,0,hub_thickness]) rotate([0,180,0]) pulley (
		 	number_of_teeth=number_of_teeth, 
			retainer=1, 
			retainer_ht=1,
			idler=1, 
			idler_ht=idler_ht,
			gear_thickness=7.1+idler_ht+retainer_ht,
			rim_thickness=rim_thickness,
			rim_width = 4.5,
			hub_thickness =hub_thickness,
			hub_diameter=hub_diameter,
			bore_diameter=5.3,
			spokeStyle="fill",
			tooth_depth=0.764,
			tooth_width =1.494 );

		translate([0,-5,-.1]) cube([m3_nut_flats,m3_nut_depth,10],center = true);
		translate([0,-1,4])rotate([0,90,-90])rotate(180/8) cylinder(r=1.7,h=hub_diameter/2,$fn=8);
		translate([0,-5,4])rotate([0,90,-90]) cylinder(r=m3_nut_diameter/2+.4,h=m3_nut_depth,$fn=6,center=true);
	}
}


module WadesS(){
	//small WADE's Gear
	echo("module WadesS");
	number_of_teeth=10;
	hub_diameter= 20;
	hub_thickness = 18;

	pitch=268;
	difference() {
			translate([0,0,hub_thickness]) rotate([0,180,0]) gear (number_of_teeth,
			circular_pitch=pitch,
			gear_thickness = 10,
			rim_thickness =10,
			hub_thickness = hub_thickness,
			hub_diameter = hub_diameter,
			bore_diameter = 5.3,
			spokeStyle="fill",
			chamfer=2,
			twist = 0);

		translate([0,-3.8,-.1]) cube([m3_nut_flats,m3_nut_depth,10],center = true);
		translate([0,-1,4])rotate([0,90,-90])rotate(180/8) cylinder(r=1.7,h=hub_diameter/2,$fn=8);
		translate([0,-3.8,4])rotate([0,90,-90]) cylinder(r=m3_nut_diameter/2+.4,h=m3_nut_depth,$fn=6,center=true);
	}
}

module WadesS_double_helix(){
	//small WADE's Gear
	spokeStyle="null";
	teeth=9;
	pitch=268;
	twist=200;
	height=25;
	pressure_angle=30;
	difference(){
		union(){
			gear (number_of_teeth=teeth,
				circular_pitch=pitch,
				pressure_angle=pressure_angle,
				clearance = 0.2,
				gear_thickness =  height/4,
				rim_thickness = height/4,
				rim_width = 4,
				hub_thickness = height/2*1.2-1.5,
				hub_diameter = 19,
				bore_diameter = 5.4,
				spokeStyle="fill",
				chamfer=1,
				twist = twist/teeth);
			mirror([0,0,1])
			gear (number_of_teeth=teeth,
				circular_pitch=pitch,
				pressure_angle=pressure_angle,
				clearance = 0.2,
				gear_thickness =  height/4*1.2,
				rim_thickness =  height/4,
				rim_width = 5,
				hub_thickness = height/4,
				hub_diameter=20,
				bore_diameter=5.4,
				spokeStyle="fill",
				twist=twist/teeth);
			difference(){
				translate([0,0,5.8]) cylinder(r=19/2, h=0.5); 
				translate([0,0,5.7]) cylinder(r=5.2/2, h=0.7); 
			}
		}
		translate([0,-5,10.7]) cube([5.5,2.3,9],center = true);
		translate([0,0,9.7])rotate([0,90,-90])cylinder(r=1.7,h=20,$fn=12);
		translate([0,-10,10])cube([10,2,11],center = true);// chanfer3
		mirror([0,0,1]) translate([0,0,5]) cylinder(r=16.5/2, h=2); // shorter
		mirror([0,0,1]) translate([0,0,3.5]) difference(){
			cylinder(r=16.5/2, h=2);
			cylinder(r1=16.5/2,r2=(16.5-3)/2,h=1);
		}
	}
}

module pulley(
		number_of_teeth=88,
		retainer=1,
		retainer_ht=1,
		idler=1,
		idler_ht=1,
		gear_thickness= 5,
		rim_thickness= 7.5,
		rim_width = 4,
		hub_thickness =5,
		hub_diameter = 19,
		bore_diameter=5.4,
		spokeStyle="fill",
		tooth_depth=0.764,
		tooth_width =1.494 )
{
	root_radius = (tooth_spacing (2,0.254,number_of_teeth))/2;
	tooth_distance_from_centre = sqrt( pow(root_radius,2) - pow((tooth_width+additional_tooth_width)/2,2));
	tooth_width_scale = (tooth_width + additional_tooth_width ) / tooth_width;
	tooth_depth_scale = ((tooth_depth + additional_tooth_depth ) / tooth_depth) ;
	if (! idler) assign(idler_ht=0);
	if (! retainer) assign(retainer_ht=0);
	pulley_b_ht = idler_ht; 	

	// Variables controlling the rim.
	rim_radius = root_radius - rim_width;
	//echo (str("teeth = ",number_of_teeth,", root_radius = ",root_radius ,", rim_radius = ",rim_radius));
	//echo (str("idler = ",idler,", idler_ht = ",idler_ht ,", retainer = ",retainer,", retainer_ht = ",retainer_ht));

	//translate ([0,0, pulley_b_ht + rim_thickness + retainer_ht ]) rotate ([0,180,0])
	difference(){	 
		union(){
			difference(){
				//shaft - diameter is outside diameter of pulley
				union(){
					translate([0,0,idler_ht]) 
						cylinder(r=root_radius,h=rim_thickness, $fn=100);
					//belt retainer / idler
					if ( retainer > 0 ) {
						translate ([0,0, idler_ht + rim_thickness ]) 
						rotate_extrude($fn=number_of_teeth*4)  
							polygon([[0,0],[root_radius+ retainer_ht/2,0],
										[root_radius+ retainer_ht  , retainer_ht/2],
										[root_radius + retainer_ht , retainer_ht],
										[0 , retainer_ht],
										[0,0]]);
					}
		
					if ( idler > 0 ) {
						translate ([0,0, 0]) 
						rotate_extrude($fn=number_of_teeth*4)  
							polygon([[0,0],[root_radius+ idler_ht,0],
										[root_radius+ idler_ht, idler_ht/2],
										[root_radius+ idler_ht/2, idler_ht],
										[0 , idler_ht],
										[0,0]]);
					}
				}		
				for(i=[1:number_of_teeth]) 
					rotate([0,0,i*(360/number_of_teeth)])
					translate([0,-tooth_distance_from_centre,idler_ht ]) 
					scale ([ tooth_width_scale , tooth_depth_scale , 1 ]) {
						GT2_2mm(rim_thickness);
					}
				//echo (str("gear_thickness = ",gear_thickness,", rim_thicknes = ",rim_thickness));

				if (gear_thickness <= rim_thickness){
					translate ([0,0,-.1]) cylinder (r=rim_radius,h=rim_thickness+idler_ht+retainer_ht+.2,$fn=number_of_teeth*4);
				}
				if (gear_thickness+1.5 < rim_thickness){
					translate([0,0,rim_thickness+idler_ht+retainer_ht-1.5]) 
						cylinder(r1=rim_radius,r2=rim_radius+2,h=2,$fn=number_of_teeth*4);
				}
				if (gear_thickness > rim_thickness)
					cylinder (r= rim_radius,h=gear_thickness);
				}
				if (rim_radius  < hub_diameter/2) {
				translate ([0,0,gear_thickness])
				rotate_extrude($fn=hub_diameter*2) {
						square([hub_diameter/2-1,hub_thickness-gear_thickness-1]);
						translate([0,1]) square([hub_diameter/2,hub_thickness-gear_thickness-1]);
						translate([hub_diameter/2-1,1]) circle(1);
				}
			
				} else {
				rotate_extrude($fn=hub_diameter*2) {
						square([hub_diameter/2-1,hub_thickness]);
						translate([0,0]) square([hub_diameter/2,hub_thickness-1]);
						translate([hub_diameter/2-1,hub_thickness-1]) circle(1);
				}
					//#cylinder (r=hub_diameter/2,h=hub_thickness);
				}		
			if ( spokeStyle == "fill" ) { 
				cylinder( h=gear_thickness, r=rim_radius);
			} else if ( spokeStyle == "biohazard" ) { 
				biohazardSpokes( rim_radius*2, gear_thickness, numberOfSpokes );
			} else if ( spokeStyle == "circlefit" ) {
				circlefitSpokes( rim_radius*2, hub_diameter, gear_thickness, rim_radius/2 );
			} else if ( spokeStyle == "line" ) {
				lineSpokes( rim_radius*2, gear_thickness, numberOfSpokes, spokeWidth );
			} else if ( spokeStyle == "rectangle" ) {
				rectangleSpokes( rim_radius*2, gear_thickness, spokeWidth, proportion, numberOfSpokes );
			} else if ( spokeStyle == "diamond" ) {
				diamondSpokes( rim_radius*2, gear_thickness, spokeWidth, proportion, numberOfSpokes );
			} else if ( spokeStyle == "circle" ) {
				circleSpokes( rim_radius*2, gear_thickness, spokeWidth, proportion, numberOfSpokes );
			} else if ( spokeStyle == "spiral" ) {
				spiralSpokes( rim_radius*2, gear_thickness, numberOfSpokes,
				spokeWidth, curvature, reverse, spiralSpoke);
			} else if ( spokeStyle == "teardrop" ){
				TeardropSpokes( rim_radius*2+.1, gear_thickness, proportion, 
								numberOfSpokes,hub_diameter,reverse );	
			} else if ( spokeStyle == "none" ){
				echo("error spokeStyle");
			}	

		}
		if(bore_diameter > 0) {
			translate ([0,0,-1])
				cylinder ( r=bore_diameter/2, h=2+max(rim_thickness,hub_thickness,gear_thickness));
		}	   
	}	   
}

module gear (
	number_of_teeth=15,
	circular_pitch=false, 
    	diametral_pitch=false,
	pressure_angle=28,
	clearance = 0.2,
	gear_thickness=5,
	rim_thickness=8,
	rim_width=5,
	hub_thickness=10,
	hub_diameter=15,
	bore_diameter=5,
	spokeStyle="fill",
	chamfer=0,
	backlash=0,
	twist=0,
	involute_facets=10)
{
	echo("module gear");
	if (circular_pitch==false && diametral_pitch==false) 
		echo("gear module needs either a diametral_pitch or circular_pitch");

	//Convert diametrial pitch to our native circular pitch
	circular_pitch = (circular_pitch!=false?circular_pitch:180/diametral_pitch);

	// Pitch diameter: Diameter of pitch circle.
	pitch_diameter  =  number_of_teeth * circular_pitch / 180;
	pitch_radius = pitch_diameter/2;

	// Base Circle
	base_radius = pitch_radius*cos(pressure_angle);

	// Diametrial pitch: Number of teeth per unit length.
	pitch_diametrial = number_of_teeth / pitch_diameter;

	// Addendum: Radial distance from pitch circle to outside circle.
	addendum = 1/pitch_diametrial;

	//Outer Circle
	outer_radius = pitch_radius+addendum;

	// Dedendum: Radial distance from pitch circle to root diameter
	dedendum = addendum + clearance;

	// Root diameter: Diameter of bottom of tooth spaces.
	root_radius = pitch_radius-dedendum;
	backlash_angle = backlash / pitch_radius * 180 / PI;
	half_thick_angle = (360 / number_of_teeth - backlash_angle) / 4;

	// Variables controlling the rim.
	rim_radius = root_radius - rim_width;

	// Variables controlling the circular holes in the gear.
	circle_orbit_diameter=hub_diameter/2+rim_radius;
	circle_orbit_curcumference=PI*circle_orbit_diameter;

	difference () {
		union () {
			difference () {
				linear_extrude (height=rim_thickness, convexity=10, twist=twist)
				gear_shape (
					number_of_teeth,
					pitch_radius = pitch_radius,
					root_radius = root_radius,
					base_radius = base_radius,
					outer_radius = outer_radius,
					half_thick_angle = half_thick_angle,
					involute_facets=involute_facets);
				if (gear_thickness <= rim_thickness){
					translate ([0,0,-.1]) cylinder (r=rim_radius,h=rim_thickness+.2);
				}
				if (gear_thickness+1.5 < rim_thickness){
					translate([0,0,rim_thickness-1.5]) cylinder(r1=rim_radius,r2=rim_radius+2,h=2);
				}
			}
			if (gear_thickness > rim_thickness)
				cylinder (r= rim_radius,h=gear_thickness);


			if (rim_radius  < hub_diameter/2) {
				translate ([0,0,gear_thickness])
				rotate_extrude($fn=hub_diameter*2) {
						square([hub_diameter/2-1,hub_thickness-gear_thickness-1]);
						translate([0,1]) square([hub_diameter/2,hub_thickness-gear_thickness-1]);
						translate([hub_diameter/2-1,1]) circle(1);
				}
			} else {
				rotate_extrude($fn=hub_diameter*2) {
						square([hub_diameter/2-1,hub_thickness]);
						translate([0,0]) square([hub_diameter/2,hub_thickness-1]);
						translate([hub_diameter/2-1,hub_thickness-1]) circle(1);
				}
			}

			if ( spokeStyle == "fill" ) { 
				cylinder( h=gear_thickness, r=rim_radius);
			} else if ( spokeStyle == "biohazard" ) { 
				biohazardSpokes( rim_radius*2, gear_thickness, numberOfSpokes );
			} else if ( spokeStyle == "circlefit" ) {
				circlefitSpokes( rim_radius*2, hub_diameter, gear_thickness, rim_radius/2 );
			} else if ( spokeStyle == "line" ) {
				lineSpokes( rim_radius*2, gear_thickness, numberOfSpokes, spokeWidth );
			} else if ( spokeStyle == "rectangle" ) {
				rectangleSpokes( rim_radius*2, gear_thickness, spokeWidth, proportion, numberOfSpokes );
			} else if ( spokeStyle == "diamond" ) {
				diamondSpokes( rim_radius*2, gear_thickness, spokeWidth, proportion, numberOfSpokes );
			} else if ( spokeStyle == "circle" ) {
				circleSpokes( rim_radius*2, gear_thickness, spokeWidth, proportion, numberOfSpokes );
			} else if ( spokeStyle == "spiral" ) {
				spiralSpokes( rim_radius*2, gear_thickness, numberOfSpokes,
				spokeWidth, curvature, reverse, spiralSpoke);
			} else if ( spokeStyle == "teardrop" ){
				TeardropSpokes( rim_radius*2+.1, gear_thickness, proportion, 
								numberOfSpokes,hub_diameter,reverse );	
			} else if ( spokeStyle == "none" ){
				echo("error spokeStyle");
			}	
		}
		translate ([0,0,-1])
		cylinder ( r=bore_diameter/2, h=2+max(rim_thickness,hub_thickness,gear_thickness));

		if (chamfer==1 || chamfer==3) {
			translate([0,0,rim_thickness-1])
				difference(){ 
					cylinder(r=outer_radius+.2, h=1.1);
					translate([0,0,-1]) cylinder(r=hub_diameter/2+.5,h=3);
					cylinder(r1=outer_radius+.2,r2=(root_radius),h=1);
				}
		}
		if (chamfer==2 || chamfer==3) {
			mirror([0,0,1])
				translate([0,0,-1]) 
					difference(){
						cylinder(r=outer_radius+.2, h=1.1);
						cylinder(r1=outer_radius+.2,r2=root_radius,h=1);
					}	
		}
	}
}


module gear_shape (
	number_of_teeth,
	pitch_radius,
	root_radius,
	base_radius,
	outer_radius,
	half_thick_angle,
	involute_facets)
{
	echo("module gear_shape");
	union() {
		circle (r=root_radius, $fn=number_of_teeth*2);
		for (i = [1:number_of_teeth]) {
			rotate ([0,0,i*360/number_of_teeth]) {
				involute_gear_tooth (
					pitch_radius = pitch_radius,
					root_radius = root_radius,
					base_radius = base_radius,
					outer_radius = outer_radius,
					half_thick_angle = half_thick_angle,
					involute_facets=involute_facets);
			}
		}
	}
}

module involute_gear_tooth (
	pitch_radius,
	root_radius,
	base_radius,
	outer_radius,
	half_thick_angle,
	involute_facets)
{
	min_radius = max (base_radius,root_radius);

	pitch_point = involute (base_radius, involute_intersect_angle (base_radius, pitch_radius));
	pitch_angle = atan2 (pitch_point[1], pitch_point[0]);
	centre_angle = pitch_angle + half_thick_angle;
	start_angle = involute_intersect_angle (base_radius, min_radius);
	stop_angle = involute_intersect_angle (base_radius, outer_radius);

	res=(involute_facets!=0)?involute_facets:($fn==0)?5:$fn/4;

	union () {
		for (i=[1:res]) {
			assign (
				point1= involute (base_radius,start_angle+(stop_angle - start_angle)*(i-1)/res),
				point2= involute (base_radius,start_angle+(stop_angle - start_angle)*i/res))
			assign (
				side1_point1=rotate_point (centre_angle, point1),
				side1_point2=rotate_point (centre_angle, point2),
				side2_point1=mirror_point (rotate_point (centre_angle, point1)),
				side2_point2=mirror_point (rotate_point (centre_angle, point2)))
			//{
				polygon (
					points=[[0,0],side1_point1,side1_point2,side2_point2,side2_point1],
					paths=[[0,1,2,3,4,0]]);
			//}
		}
	}
}



// Spoke Styles...  
// Teardrop pattern spokes
module TeardropSpokes( wheelDiameter, wheelWidth, proportion, numberofSpokes,hub_diameter,reverse ) {
	echo( "Teardrop Style..." ); 
	difference() {
		translate ([0,0,wheelWidth/2])  cylinder( h=wheelWidth, r=wheelDiameter/2, center = true ); 
		for (step = [0:numberofSpokes-1]) {
			translate ([0,0,wheelWidth/2])	
		    	rotate( [0, 0, step*(360/numberofSpokes)] )
			TeardropSpoke(  wheelDiameter, wheelWidth, proportion,hub_diameter,reverse );
		}
	}

}
module TeardropSpoke( wheelDiameter, wheelWidth, proportion, hub_diameter,reverse) {
	a=((wheelDiameter/2-hub_diameter/2)/2)*.90;
	dia_Teardrop=wheelDiameter/2-((wheelDiameter/2-hub_diameter/2)/2);
	translate ( [dia_Teardrop, 0, -wheelWidth/2-1] ) {
		union() {
			scale([proportion[0],proportion[1],1])
				cylinder(r=a,h=wheelWidth+2);
			if(reverse){
				scale([proportion[0],proportion[1],1])
				rotate(-135) cube([a,a,wheelWidth+2]);
			} else {
				scale([proportion[0],proportion[1],1])
				rotate(45) cube([a,a,wheelWidth+2]);
			}
		}
	}
}

//Diamonds pattern spokes
module diamondSpokes( wheelDiameter, wheelWidth, lineWidth, proportion, numberofSpokes ) {
	echo( "Diamonds Style..." ); 
	intersection() {
		translate ([0,0,wheelWidth/2]) cylinder( h=wheelWidth, r= wheelDiameter/2, center = true ); 
		for (step = [0:numberofSpokes-1]) {
			translate ([0,0,wheelWidth/2])		    
			rotate( a=step*(360/numberofSpokes), v=[0, 0, 1])
			diamondSpoke( wheelDiameter, wheelWidth, lineWidth, proportion );
		}
	}
}
module diamondSpoke( wheelDiameter, wheelWidth, lineWidth, proportion ) {

	// Let's make the lines the correct thickness even after the transformations 
	// are made... Maybe there's a better way? 
	assign( leg=sqrt( 2*pow(wheelDiameter/4,2 ) ) ) // Euclid rocks. 
	assign( p=(wheelDiameter/2)*proportion[0], q=(wheelDiameter/2)*proportion[1] )
	assign( a=sqrt((p*p)/4 + (q*q)/4) ) 
	assign( h=(p*q)/(2*a) )
	assign( theta=2*asin(q/(2*a)) ) 
	assign( prop=( 2*cos(theta/2)*(h-(2*lineWidth)) ) / ( sin(theta)*p ) ) {
		translate ( [-p/2, 0, 0] ) {
			difference() {
				scale([proportion[0],proportion[1],1]) {
					rotate([0,0,45])
						cube( [leg, leg, wheelWidth], center=true); 
				}

				scale([prop,prop,1]) {
					scale([proportion[0],proportion[1],1]) {
						rotate([0,0,45])  // My wife is the epitome of awesomeness. 
							cube( [leg, leg, wheelWidth+1], center=true); 
					}
				}
			}
		}
	}
}

// Circles pattern spokes
module circleSpokes( wheelDiameter, wheelWidth, lineWidth, proportion, numberofSpokes ) {
	echo( "Circles Style..." ); 
	intersection() {
		translate ([0,0,wheelWidth/2])  cylinder( h=wheelWidth, r=wheelDiameter/2, center = true ); 

		for (step = [0:numberofSpokes-1]) {
			translate ([0,0,wheelWidth/2])		    
			rotate( [0, 0, step*(360/numberofSpokes)] )
			circleSpoke(  wheelDiameter, wheelWidth, lineWidth, proportion );
		}
	}
}

module circleSpoke( wheelDiameter, wheelWidth, lineWidth, proportion ) {
	ox=(wheelDiameter/2)*proportion[0];
	oy=(wheelDiameter/2)*proportion[1];
	ix=ox-(lineWidth*2);
	iy=oy-(lineWidth*2) ;
	
	translate ( [-ox/2, 0, 0] ) {
		difference() {
			scale([proportion[0],proportion[1],1]) 
				cylinder( r=wheelDiameter/4, h=wheelWidth, center=true); 
			scale([(ix/ox)*proportion[0],(iy/oy)*proportion[1],1]) 
				cylinder( r=wheelDiameter/4, h=wheelWidth +1, center=true); 
		}
	}
}

//Rectangle pattern spokes
module rectangleSpokes( wheelDiameter, wheelWidth, lineWidth, proportion, numberofSpokes ) {
	echo( "Rectangles Style..." ); 
	intersection() {
		translate ([0,0,wheelWidth/2]) cylinder( h=wheelWidth, r= wheelDiameter/2, center = true ); 

		for (step = [0:numberofSpokes-1]) {
			translate ([0,0,wheelWidth/2]) 		    
			rotate( a = step*(360/numberofSpokes), v=[0, 0, 1])
			rectangleSpoke(  wheelDiameter, wheelWidth, lineWidth, proportion );
		}
	}
}
module rectangleSpoke( wheelDiameter, wheelWidth, lineWidth, proportion ) {
	assign( ox=(wheelDiameter/2)*proportion[0], oy=(wheelDiameter/2)*proportion[1] )
	assign( ix=ox-(lineWidth*2), iy=oy-(lineWidth*2) ) {
		translate ( [-ox/2, 0, 0] ) {
			difference() {
				cube( [ox, oy, wheelWidth], center=true); 
				cube( [ix, iy, wheelWidth+1], center=true); 
			}
		}
 	}
}

// Spiral pattern spokes 
module spiralSpokes( diameter, wheelWidth, number, spokeWidth, curvature, reverse ) {
	echo( "Spiral Style..." ); 
	intersection() {
		translate ([0,0,wheelWidth/2]) cylinder( h=wheelWidth, r=diameter/2, center = true ); 

		for (step = [0:number-1]) {
		    rotate( a = step*(360/number), v=[0, 0, 1])
			translate ([0,0,wheelWidth/2]) spiralSpoke( wheelWidth, spokeWidth, (diameter/4) * 1/curvature, reverse );
		}
	}
}
module spiralSpoke( wheelWidth, spokeWidth, spokeRadius, reverse=false ) {
	intersection() {	
		translate ( [-spokeRadius, 0, 0] ) {
			difference() {
				cylinder( r=spokeRadius, h=wheelWidth, center=true ); 
				cylinder( r=spokeRadius-(spokeWidth/2), h=wheelWidth+1, center=true ); 
			}
		}
		if ( reverse ) 
			translate ( [-spokeRadius, -spokeRadius/2, 0] ) 
				cube( [spokeRadius*2,spokeRadius,wheelWidth+1], center=true ); 
		else 
			translate ( [-spokeRadius, spokeRadius/2, 0] ) 
				cube( [spokeRadius*2,spokeRadius,wheelWidth+1], center=true ); 
	}
}

// Biohazard pattern spokes
module biohazardSpokes( diameter, width, number ) {
	echo( "Biohazard Style..." ); 
	scale = (diameter/2) / 88;
	intersection() {
		translate ([0,0,width/2])  cylinder( h=width, r= diameter/2, center = true ); 
		scale( [scale, scale, 1] ) {
			for (step = [0:number-1]) {
			    rotate( a = step*(360/number), v=[0, 0, 1])
				translate ([0,0,width/2]) biohazardSpoke( width );
			}
		}
	}

}
module biohazardSpoke( width ) {
	translate ( [-60, 0, 0] )
	difference() {
		translate ( [10, 0, 0] )  cylinder( h = width, r= 50, center=true ); 
		translate ( [-1, 0, 0] )  cylinder( h = width+2, r= 40, center=true ); 
	}
}

// Circlefit pattern spokes 
module circlefitSpokes( diameter, hubDiameter, width, outerHoleDiameter ) {
	echo( "Circlefit Style..." ); 
	padding = 2; 
	paddedHoleRadius = (diameter-hubDiameter)/4;
	holeRadius = paddedHoleRadius - padding/2;
	hubRadius = hubDiameter/2; 

	// Figure out how many circles will fit. 
	circles = floor(360/(2*(asin(paddedHoleRadius/(paddedHoleRadius+hubRadius)))));
	difference() {
		translate ([0,0,width/2])  cylinder( h=width, r= diameter/2, center = true ); 

		for ( i = [0 : circles-1] ) {
		   	 rotate( i * (360/circles), [0, 0, 1])
		   	 translate([hubRadius + paddedHoleRadius, 0, width/2])
		   	 cylinder(h=width+1, r=holeRadius, center=true);
		}
	}
}

// Line pattern spokes
module lineSpokes( diameter, wheelWidth, number, spokeWidth ) {
	echo( "Lines Style..." );
	intersection() {
		translate ([0,0,wheelWidth/2]) cylinder( h=wheelWidth, r= diameter/2, center = true ); 

		for (step = [0:number-1]) {
		    rotate( a = step*(360/number), v=[0, 0, 1])
			translate ([0,0,wheelWidth/2]) lineSpoke( wheelWidth, spokeWidth );
		}
	}
}

module lineSpoke( wheelWidth, spokeWidth ) {
	translate ( [-60/2, 0, 0] )
	cube( [60, spokeWidth, wheelWidth], center=true); 
}

module m8_hole_vert(l) {
	cylinder(l,m8_clearance_rad,m8_clearance_rad,center=true);
}

module m8_hole_vert_with_hex(l) {
	union () {
		m8_hole_vert(l);
		translate ([0,0,-l/4]) rotate ([0,0,30]) m8_nut_cavity(l/2);
	}
}

module m8_nut_cavity(l) {
	hexagon(height=13.8,depth=l); // [am] tight fit
}

// Mathematical Functions

function involute_intersect_angle (base_radius, radius) = sqrt (pow (radius/base_radius, 2) - 1) * 180 / PI;

// Calculate the involute position for a given base radius and involute angle.

function rotated_involute (rotate, base_radius, involute_angle) = 
[
	cos (rotate) * involute (base_radius, involute_angle)[0] + sin (rotate) * involute (base_radius, involute_angle)[1],
	cos (rotate) * involute (base_radius, involute_angle)[1] - sin (rotate) * involute (base_radius, involute_angle)[0]
];

function mirror_point (coord) = [coord[0], -coord[1]];

function rotate_point (rotate, coord) =
[
	cos (rotate) * coord[0] + sin (rotate) * coord[1],
	cos (rotate) * coord[1] - sin (rotate) * coord[0]
];

function involute (base_radius, involute_angle) = 
[
	base_radius*(cos (involute_angle) + involute_angle*PI/180*sin (involute_angle)),
	base_radius*(sin (involute_angle) - involute_angle*PI/180*cos (involute_angle)),
];



function tooth_spaceing_curvefit (b,c,d) = ((c * pow(teeth,d)) / (b + pow(teeth,d))) * teeth ;

module GT2_2mm(rim_thickness)
	{
	linear_extrude(height=rim_thickness) polygon([[0.747183,-0.5],[0.747183,0],[0.647876,0.037218],[0.598311,0.130528],[0.578556,0.238423],[0.547158,0.343077],[0.504649,0.443762],[0.451556,0.53975],[0.358229,0.636924],[0.2484,0.707276],[0.127259,0.750044],[0,0.76447],[-0.127259,0.750044],[-0.2484,0.707276],[-0.358229,0.636924],[-0.451556,0.53975],[-0.504797,0.443762],[-0.547291,0.343077],[-0.578605,0.238423],[-0.598311,0.130528],[-0.648009,0.037218],[-0.747183,0],[-0.747183,-0.5]]);
	}

/*
module GT2_2mm(thickness=1){
	union(){
		difference(){
			union(){
				hull(){
				translate ([0,.21,0]) cylinder(r=.555,h=thickness,$fn=10);
				translate ([-1.205/2,.025,0]) cube([1.205,.1,thickness]);
				}
			translate ([-1.5/2,-.5,0]) cube([1.5,.63,thickness]);

			}
			translate ([.75,.15,-.1]) cylinder(r=.15,h=thickness+12,$fn=10);
			translate ([-.75,.15,-.1]) cylinder(r=.15,h=thickness+.2,$fn=10);
		}
	}
}
*/
module test(){

WadesL(); 
translate ([0,45]) WadesS(); 

//WadeL_double_helix();
//rotate([0,180,0]) WadesS_double_helix();

//translate ([55,45])WadebeltL();
//translate ([55,0])WadebeltS();
//translate ([55,-30])bearing_guide();
}