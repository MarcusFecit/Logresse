//   Wade for P3STEEL
//   Patrick CARON
//   Version 3.02	20/07/2014
// Derived from: Greg's Wade
// Licensed under the GNU GPL license.
//
$fn=50;
$fs=0.1;
// Def type nozzle
malcolm_hotend_mount=1;
groovemount=2;
peek_reprapsource_mount=4;
arcol_mount=8;
mendel_parts_v6_mount=16;
grrf_peek_mount=32;
wildseyed_mount=64;
geared_extruder_nozzle=128; 		// http://reprap.org/wiki/Geared_extruder_nozzle
jhead_mount=256;
reprapfaborg_mount=512;   		// http://reprap-fab.org hotend with 10mm PEEK insulator
head_conical_screw=1024; 		// for conical M3 screw (or wood screw 2.8x25mm (BR)
standard_mount=2048;       		// This is the standard 16mm Adrian Extruder mount  
p3steel_mount=4096;			// for jhead,buda,e3d 

// ===============================
// ====             User configurations               ====
// ===============================

layer_thickness=0.25;
nozzle_wall=0.35;	
hex_base_nuts=true;				// remove hex nuts on base if top mounting screw
nema = 0;  						// 0 = nema17, 1 = nema14
688_tractor = false;				// 688 bearing replace 608  
filament_diameter=3;				// 3 or 1.75 mm, filament diameter
filament_feed_hole_extra_offset=0;	// increase if tractor with deep grove, -0.5 to 1.0
hotend_mount=p3steel_mount;	//malcolm_hotend_mount,groovemount,peek_reprapsource_mount
p3steel_mount_buda=false;	// select and hotend_mount=p3steel_mount
p3steel_mount_e3d=false;		// select and hotend_mount=p3steel_mount
p3steel_mount_jhead=false;	// select and hotend_mount=p3steel_mount
//p3_steel_universel=10-4;		// if p3_steel_universel = 0 no print 
p3_steel_universel=0;			// if p3_steel_universel = 0 no print 

print_wade=true;				// print wade  false/true
print_wadeidler=false;			// print wadeidler  false/true
print_lever=false;				// print leve  false/true
print_bar=false;				// print bar  false/true
print_object=false; 				// include reference object in the view no use for print 
support= true;
// ===============================
// RENDER EXTRUDER 
if (print_wade) rotate ([90,0,0]) wade();
if (print_wadeidler && ! print_object) translate([0,-10,idler_fulcrum[2]+1.5]) rotate([0,-90,0]) wadeidler();
if (print_lever && ! print_object) translate([40,45,0])  lever();
if (print_bar && ! print_object) translate([18,5,0]) rotate([0,90,90]) bar();
if (hotend_mount==p3steel_mount) {
  if (p3steel_mount_buda && p3steel_mount_e3d) {
	echo("<b>Error</b> p3steel_mount_e3d or p3steel_mount_buda");
	}
  if (p3_steel_universel>0 ) translate([18,40,0])  universel(p3_steel_universel);
  if (p3steel_mount_e3d && p3_steel_universel==0 && ! p3steel_mount_buda) translate([18,40,0])  p3steel_nozzle_e3d();
  if (p3steel_mount_buda && p3_steel_universel==0&& ! p3steel_mount_e3d) translate([18,40,0])  p3steel_nozzle_buda();
}
//===================================================
// Parameters defining the wade body:
//Set motor- and bolt-elevation for additional clearance when using e.g. 9/47 gears 
motor_elevation=0;
tractor_elevation=0;
//Set extra gear separation when using slightly bigger non-standard gears like 9/47 
extra_gear_separation=2;

wade_block_height=55+tractor_elevation;
wade_block_width=24;
wade_block_depth=28;
wade_tractor_translation=[11,34+tractor_elevation,0];
wade_holes=8;
block_bevel_r=5;

//  Parameter lever
lever_width=8;
lever_extra_width=0.0;
lever_inclination=7;
lever_hole=9.85;
lever_corner=[10,2.9,0];

// Parameter bar
bar_diameter=9.4;
bar_radiusr=bar_diameter/2;
bar_length=26;
bar_spring=7+0.2;

// Nut wrench sizes ISO 4032

m3_wrench = 5.5;
m3_diameter = 3.3;
m3_nut_diameter = 6.1;

m4_wrench = 7;
m4_diameter = 4.5;
m4_nut_diameter = 9;

m8_wrench = 12;
m8_diameter = 9;
m8_clearance_hole=8.8;

// Baering 
608_diameter=22;
hole_for_608=608_diameter+.2;
608_height=7;

688zz_diameter=16;
hole_for_688zz=688zz_diameter+.2;
688zz_height=5;

// Adjust for deeper groove in hobbed bolt, so that idler is still vertical when tightened
// Values like 0.5 to 1 should work, the more, the closer the idler will move to the bolt
// Sometimes the idler will be slightly angled towards the bolt which causes the idler screws
// to slip off the slots in die idler to the top.. Adjusting this should help:
less_idler_bolt_dist = 0;

base_thickness=7;
base_length=70-base_thickness;
base_leadout=25;

// Parameter  Nema
nema_dim = [[31,42.3,23,38],[26,35.2,23,33]];
nema_hole_spacing=nema_dim[nema][0];
nema_width=nema_dim[nema][1];
nema_Height=nema_dim[nema][3];
motor_center_hole=nema_dim[nema][2];

//nema_support_d=nema_width-nema_hole_spacing-1.0;
nema_support_d=10.5;
screw_head_recess_diameter=7.5;
screw_head_recess_depth=4;

motor_mount_thickness=9; 	//8;  mottor better aligned for gear position
motor_mount_rotation=-30;   	//25 allow clearing for top mount hex key
motor_mount_translation=[50+extra_gear_separation,29+motor_elevation,0];
motor_slot_left=3;
motor_slot_right=0;    		//  heringbone need extra spce to be removed

// calculate feed hole: 3=>3.5 or 1.75=>2
filament_feed_hole_d=filament_diameter*1.2;

filament_feed_hole_offset=filament_diameter+1.5-filament_feed_hole_extra_offset;
idler_nut_trap_depth=7.3;
idler_nut_thickness=3.3;
idler_nut_wrench=7;
gear_separation=7.4444+32.0111+0.25+extra_gear_separation;

// Function 
function in_mask(mask,value)=(mask%(value*2))>(value-1);

function triangulate (point1, point2, length1, length2) =
point1 +
length1*rotated(
atan2(point2[1]-point1[1],point2[0]-point1[0])+
angle(distance(point1,point2),length1,length2));

function distance(point1,point2)=
sqrt((point1[0]-point2[0])*(point1[0]-point2[0])+
(point1[1]-point2[1])*(point1[1]-point2[1]));

function angle(a,b,c) = acos((a*a+b*b-c*c)/(2*a*b));

function rotated(a)=[cos(a),sin(a),0];

function motor_hole(hole)=[motor_mount_translation[0],motor_mount_translation[1]] +
	rotated(45+motor_mount_rotation+hole*90)*nema_hole_spacing/sqrt(2);

// Parameters defining the idler.

filament_pinch=[filament_feed_hole_offset, wade_tractor_translation[1], wade_block_depth/2];
idler_axis=filament_pinch-[608_diameter/2-1, 0, 0];
idler_fulcrum_offset=608_diameter/2+3.5+m3_diameter/2+0.5;
idler_fulcrum=idler_axis-[less_idler_bolt_dist, idler_fulcrum_offset, 0];
idler_corners_radius=2.5; // was 4
idler_height=12;
idler_608_diameter=608_diameter+2;
idler_608_height=9;
idler_mounting_hole_across=8;
idler_mounting_hole_up=15;
idler_short_side=wade_block_depth-2;
idler_hinge_re=m3_diameter/2+3.5;   // radius extruder side
idler_hinge_rh=m3_diameter/2+3.7;   // radious idler side
idler_hinge_width=6.5;
idler_end_length=(idler_height-2)+5;
idler_mounting_hole_diameter=m4_diameter+0.25;
idler_mounting_hole_elongation=0.9;
idler_long_top=idler_mounting_hole_up+idler_mounting_hole_diameter/2+idler_mounting_hole_elongation+2.5;
idler_long_bottom=idler_fulcrum_offset;
idler_long_side=idler_long_top+idler_long_bottom;

// Module 
module wade (){
  difference(){
    union(){
	difference (){
		union(){
			// The wade block.
			cube([wade_block_width,wade_block_height,wade_block_depth]);

			//The base.lever
			translate([-base_leadout,0,0])
			 	cube([base_length,base_thickness,wade_block_depth]);
			translate([wade_tractor_translation[0]-filament_feed_hole_offset,0,wade_block_depth/2]) 
				rotate ([-90,0,0]) cylinder(r=20/2,h=11);
			translate([wade_tractor_translation[0]-filament_feed_hole_offset,11,wade_block_depth/2]) 
				rotate ([-90,0,0]) cylinder(r1=20/2,r2=16/2,h=2);
			//Connect motor mount to base.
			hull(){
				translate([motor_hole(3)[0]+motor_slot_right,motor_hole(3)[1],0])
					cylinder(r=nema_support_d/2,h=motor_mount_thickness);
				translate([motor_hole(3)[0]-motor_slot_right,base_thickness/2,0])
					cylinder(r=base_thickness/2,h=motor_mount_thickness);
				}

			hull(){
				translate([motor_hole(2)[0]+motor_slot_right,motor_hole(2)[1],0])
					cylinder(r=nema_support_d/2,h=motor_mount_thickness);
				translate([wade_block_width-5,0,0])
					cube([5,wade_block_height,motor_mount_thickness]); 
				}
			hull(){
				translate([motor_hole(1)[0]+motor_slot_right,motor_hole(1)[1],0])
					cylinder(r=nema_support_d/2,h=motor_mount_thickness);
				translate([motor_hole(2)[0]+motor_slot_right,motor_hole(2)[1],0])
					cylinder(r=nema_support_d/2,h=motor_mount_thickness);
				}
			hull(){
				translate([motor_hole(2)[0]+motor_slot_right,motor_hole(2)[1],0])
					cylinder(r=nema_support_d/2,h=motor_mount_thickness);
				translate([motor_hole(3)[0]-motor_slot_left,motor_hole(3)[1],0])
					cylinder(r=nema_support_d/2,h=motor_mount_thickness);
				}

			hull(){
				translate([motor_hole(1)[0]-motor_slot_left,motor_hole(1)[1],0])
					cylinder(r=nema_support_d/2,h=motor_mount_thickness);
				translate([wade_block_width-3.5,0,0])
					cube([5,wade_block_height+1.5,motor_mount_thickness]); 
				}
			hull(){
				translate([motor_hole(3)[0]-motor_slot_left,motor_hole(3)[1],0])
					cylinder(r=nema_support_d/2,h=motor_mount_thickness);
				translate([wade_block_width-3.5,0,0])
					cube([5,wade_block_height*2/3,motor_mount_thickness]); 
				}
			translate([motor_hole(3)[0]-motor_slot_right-30,0,0])
			 	cube([30,base_thickness,motor_mount_thickness]); 

			// Round the ends of the base
			translate([base_length-base_leadout,base_thickness/2,0])
			   cylinder(r=base_thickness/2,h=wade_block_depth);
			translate([base_length-base_leadout,0,0])
				cube([base_thickness/2,base_thickness/2,wade_block_depth]); 

			translate([-base_leadout,base_thickness/2,0])
				cylinder(r=base_thickness/2,h=wade_block_depth);
			translate([-base_leadout-base_thickness/2,0,0])
				cube([base_thickness/2,base_thickness/2,wade_block_depth]); 

			//Provide the bevel betweeen the base and the wade block.
			union()
			 difference(){
				translate([-block_bevel_r,0,0])
				  cube([block_bevel_r*2+wade_block_width, base_thickness+block_bevel_r,wade_block_depth]);
				translate([-block_bevel_r,block_bevel_r+base_thickness,-0.1])
				  cylinder(r=block_bevel_r,h=wade_block_depth+0.2);
				translate([wade_block_width+block_bevel_r, block_bevel_r+base_thickness,-0.1])
				  cylinder(r=block_bevel_r,h=wade_block_depth+0.2);
			 }
			// The idler hinge.
			translate(idler_fulcrum){
				rotate(5) 
			           translate([idler_hinge_re+1,0,0])
				      cube([idler_hinge_re*2+1,idler_hinge_re*2,idler_short_side-2*idler_hinge_width-.5], center=true);
				cylinder(r=idler_hinge_re, h=idler_short_side-2*idler_hinge_width-0.5, center=true);
				rotate(-30)			
				   translate([idler_hinge_re+2,0,0])
				      cube([idler_hinge_re*2+4,idler_hinge_re*2,  idler_short_side-2*idler_hinge_width-.5], center=true);
			}


    			for (hole=[1:3]) hull(){ 
				translate([motor_hole(hole)[0]-motor_slot_left,motor_hole(hole)[1],0])		
					cylinder(r=nema_support_d/2,h=motor_mount_thickness);
				translate([motor_hole(hole)[0]+motor_slot_right,motor_hole(hole)[1],0])
					cylinder(r=nema_support_d/2,h=motor_mount_thickness);
    			}

			// extra 1.5mm reinforcement on top,extra 2mm reinforcement on right 
			translate([13.0,wade_block_height-13.5+1.5,0]) cube([13,13.5,wade_block_depth]);
			// left ramp the first part is here to get proper screw hole
			translate([13.0,wade_block_height+1.5,0]) rotate([0,0,-158]) 
              		  cube([4.5,3,wade_block_depth]);
			translate([wade_block_width+.26,wade_block_height-13.5+2.5,0]) rotate([0,0,-120]) 
               		  cube([4,2,wade_block_depth]);

	        	if ( 688_tractor) {
				// Back Reinforcement 
	        		translate(wade_tractor_translation)
	        		translate([0,0,wade_block_depth-10])
	        		difference(){
	         			union(){
		        			cylinder(r=30/2,h=10);
		         			mirror([0,0,1])
		         			cylinder(r1=30/2,r2=27/2,h=3);
	          			}
	          			translate([-17,-18,-4]) cube([30,36,20]);
	        		}
			}	
		}

		block_holes();
		motor_mount_holes ();

		translate([wade_tractor_translation[0]-filament_feed_hole_offset,0,wade_block_depth/2])
		rotate([-90,0,0]){
			if (in_mask (hotend_mount,malcolm_hotend_mount)) malcolm_hotend_holes ();
			if (in_mask (hotend_mount,groovemount)) groovemount_holes ();
			if (in_mask (hotend_mount,peek_reprapsource_mount)) peek_reprapsource_holes ();
			if (in_mask (hotend_mount,arcol_mount)) arcol_mount_holes ();
			if (in_mask (hotend_mount,mendel_parts_v6_mount)) mendel_parts_v6_holes(insulator_d=12.7);
			if (in_mask(hotend_mount,grrf_peek_mount)) grrf_peek_mount_holes();
			if (in_mask(hotend_mount,wildseyed_mount)) wildseyed_mount_holes(insulator_d=12.7,wildseyed_fix=true);
			if (in_mask(hotend_mount,geared_extruder_nozzle)) mendel_parts_v6_holes(insulator_d=15);
			if (in_mask(hotend_mount,jhead_mount)) wildseyed_mount_holes(insulator_d=16,wildseyed_fix=true);
			if (in_mask(hotend_mount,head_conical_screw)) jh_conical_screw();
			if (in_mask(hotend_mount,reprapfaborg_mount)) peek_reprapfaborg_holes();
			if (in_mask(hotend_mount,p3steel_mount)) p3steel_mount_holes(insulator_d=16,wildseyed_fix=p3steel_mount_buda);

		}
	}
	translate([wade_tractor_translation[0]-filament_feed_hole_offset,0,wade_block_depth/2]) 
		rotate ([-90,0,0]) {

		if (in_mask (hotend_mount,malcolm_hotend_mount)) {
	    		translate ([0,0,3.7])
	  	  	  cylinder(r=16/2,h=.4);  // [smf]
          		for (a=[0:17]) {
			  rotate ([0,0,20*a])
              		    cube ([.2, 16.4/2, 3.6]);
          		}
	  	}
	  	if (in_mask (hotend_mount,groovemount)) {
	    		translate ([0,0,4.6]) 
	  	  	  cylinder(r=16.9/2,h=.2);  // [smf]
          		for (a=[0:17]) {
				rotate ([0,0,20*a])
              			  cube ([.2, 16.4/2, 4.6]);
          		}
	  	} 
	  	if (in_mask (hotend_mount,p3steel_mount)) {
	    		translate ([0,0,10]) 
	  	  	 	cylinder(r=16/2,h=layer_thickness);
          		for (a=[0:8]) {
				rotate ([0,0,40*a])
              			  cube ([nozzle_wall, 15.6/2, 10-layer_thickness]);
          		}
		}
	}

	if (tractor_elevation > 0) {
		translate([-wade_block_depth+10,base_thickness-.2,5])
			rotate([-90,45,0]) 
				cylinder(r1=6,r2=6-(tractor_elevation/2),h=tractor_elevation,$fn=4);
		translate([-wade_block_depth+10,base_thickness-.2,wade_block_depth-5])
			rotate([-90,45,0]) 
			cylinder(r1=6,r2=6-(tractor_elevation/2),h=tractor_elevation,$fn=4);

	}
 } 

	//Round off the top back of the block.
	translate([0,wade_block_height-block_bevel_r,-.1])
	difference(){
		translate([-1,0,0])
		cube([block_bevel_r+1,block_bevel_r+1,wade_block_depth+.2]);
		translate([block_bevel_r,0,0])
		cylinder(r=block_bevel_r,h=wade_block_depth+.2);
	}

  } // of first difference    screw_head_recess_diameter/2  screw_head_recess_depth motor_mount_thickness 
	if (support){
		for (a=[0:2]){
			translate ((motor_hole(3))) translate ([.3-2*a,-screw_head_recess_diameter/2]) 
				cube([nozzle_wall,screw_head_recess_diameter-layer_thickness,screw_head_recess_depth-.2]);
			translate ((motor_hole(2))) translate ([.3-2*a,-screw_head_recess_diameter/2])
				cube([nozzle_wall,screw_head_recess_diameter-layer_thickness,screw_head_recess_depth-.2]);
			translate ((motor_hole(1))) translate ([.3-2*a,-screw_head_recess_diameter/2])
				cube([nozzle_wall,screw_head_recess_diameter-layer_thickness,screw_head_recess_depth-.2]);
			translate ((motor_hole(3))) translate ([.3-2*a,-m3_diameter/2]) 
				cube([nozzle_wall,m3_diameter-layer_thickness,motor_mount_thickness]);
			translate ((motor_hole(2))) translate ([.3-2*a,-m3_diameter/2])
				cube([nozzle_wall,m3_diameter-layer_thickness,motor_mount_thickness]);
			translate ((motor_hole(1))) translate ([.3-2*a,-m3_diameter/2])
				cube([nozzle_wall,m3_diameter-layer_thickness,motor_mount_thickness]);
		}
	}
	translate ([0,30,0]) cube([.25,13,6.8]); 
	translate ([0,30,wade_block_depth-6.8]) cube([.25,13,6.8]);
	if (print_object) { 	// include reference object in the view
		%wadeidler();
		translate(motor_mount_translation+[0,0,motor_mount_thickness])
			rotate([0,180,motor_mount_rotation])
			%stepperMotor(nema_width, nema_Height, nema_hole_spacing, 3, 5, 24, 22, 2);
		translate([wade_tractor_translation[0]-filament_feed_hole_offset,-30,wade_block_depth/2]) 
			rotate([-90,0,0])
			%nozzle_e3d();
	}
}

//block_holes();
module block_holes(){
	// Round the right front corner.
	translate ([-base_leadout-base_thickness/2,-1,wade_block_depth-block_bevel_r])
	difference(){
	  translate([-1,0,0])
	    cube([block_bevel_r+1,base_thickness+2,block_bevel_r+1]);
	  rotate([-90,0,0])
		translate([block_bevel_r,0,-1])
		cylinder(r=block_bevel_r,h=base_thickness+4);
	}
	// Round the right botton  corner.
	translate ([base_length-base_leadout+base_thickness/2-block_bevel_r,
		-1,wade_block_depth-block_bevel_r])
	difference(){
	  translate([0,0,0])
	    cube([block_bevel_r+1,base_thickness+2,block_bevel_r+1]);
	  rotate([-90,0,0])
	    translate([0,0,-1])
		cylinder(r=block_bevel_r,h=base_thickness+4);
	}
	// Round the bottom front corner.
	translate ([-base_leadout-base_thickness/2,-1,0])
	difference(){
	  translate([-1,0,-1])
		cube([block_bevel_r+1,base_thickness+2,block_bevel_r+1]);
	  rotate([-90,0,0])
		translate([block_bevel_r,-block_bevel_r,-1])
		cylinder(r=block_bevel_r,h=base_thickness+4);
	}
	// Idler fulcrum hole.
	translate(idler_fulcrum+[0,0,-0.1])
		cylinder(r=m3_diameter/2,h=idler_short_side-2*idler_hinge_width,center=true);

	//Rounded cutout for idler hinge.
	difference(){
      		translate(idler_fulcrum)
        	 	cylinder(r=idler_hinge_rh+1.5,h=idler_short_side+4,center=true);
      		translate(idler_fulcrum)
        	  	cylinder(r=idler_hinge_rh+2,h=idler_short_side-2*idler_hinge_width-0.5,center=true);
	}

	translate(wade_tractor_translation){

        	// Opening for the tractor screw
		cylinder(r=m8_clearance_hole/2,h=wade_block_depth);

	 	// Open the top to remove overhangs and to provide access to the hobbing. 
		difference(){
			union(){
				translate([-12.2,0,wade_block_depth/2-wade_holes/2])
		 			cube([wade_block_width/2+3,14,wade_holes]);
				// Open extra 20° access to tractor 
				translate([m8_clearance_hole/2,0,wade_block_depth/2-wade_holes/2]) rotate([0,0,100])
					cube([wade_block_width-13,7.5,wade_holes]);
			}
			translate([-5.2,14,wade_block_depth/2-wade_holes/2]) rotate([45,0,0]) cube([16,5,5],center=true);
			translate([-5,14,wade_block_depth/2+wade_holes/2]) rotate([45,0,0]) cube([16,5,5],center=true);
		}
		translate([-15,0,wade_block_depth/2-wade_holes/2]) b608(height=wade_holes);

        	// left bearing 
		if (688_tractor) {
			translate([0,0,-0.1]) b608(height=7.1); 
			translate([0,0,608_height-0.1]) cylinder(r1=16/2,r2=14/2,h=0.4);
		} else {
			translate([0,0,-0.1]) b688(height=5.1); 
			translate([0,0,688zz_height-0.1]) cylinder(r1=13/2,r2=11/2,h=0.4);
		}

		// right bearing
		if (688_tractor) {
        		translate([0,0,wade_block_depth-7]) b608(height=7.1);
			translate([0,0,wade_block_depth-7.4]) cylinder(r2=16/2,r1=14/2,h=0.4); 
		} else {
        		translate([0,0,wade_block_depth-5]) b688(height=5.1);
			translate([0,0,wade_block_depth-5.3]) cylinder(r2=13/2,r1=11/2,h=0.4); 
		}

		// Filament feed.
		translate([-filament_feed_hole_offset,0,wade_block_depth/2])
			rotate([90,0,0])
			cylinder(r=filament_feed_hole_d/2,h=wade_block_depth*3,center=true,$fn=64);
		translate([-filament_feed_hole_offset,wade_block_height-wade_tractor_translation[1]-1,wade_block_depth/2])
			rotate([90,0,0])
			cylinder(r2=filament_feed_hole_d/2,r1=filament_feed_hole_d/2+3,h=3,center=true,$fn=64);

		// Mounting holes on the base.
		translate([0,0,0])
			for (mount=[0:1]){
				translate([-filament_feed_hole_offset+25*((mount<1)?1:-1),
					-wade_tractor_translation[1]-1,wade_block_depth/2])
					rotate([-90,0,0]) rotate(360/16)
					cylinder(r=m4_diameter/2,h=base_thickness+2,$fn=12);

				if (hex_base_nuts){ // remove hex nuts un base if top mounting screw [am]
				    translate([-filament_feed_hole_offset+25*((mount<1)?1:-1),
					    -wade_tractor_translation[1]+base_thickness/2, wade_block_depth/2])
						rotate([-90,0,0]){
				    		   translate([0,0,base_thickness/2]) rotate([0,0,30]) 
						     nut_trap(m4_wrench,base_thickness,0);
						}
                } else {

               	if (mount==0) {  // just a surface for the washer
				translate([-filament_feed_hole_offset+25*((mount<1)?1:-1),
					-wade_tractor_translation[1]+base_thickness, wade_block_depth/2])
				rotate([-90,0,0]) cylinder (r=12/2, h=4);
             			// Allen wrench for top side access
				translate([wade_block_width-3.5,-wade_tractor_translation[1]+base_thickness,wade_block_depth/2]){
					rotate([-90,0,9]) cylinder(r=6/2,h=60,$fn=12);// this is a simulates key
					rotate([-90,0,8])  translate([2,0,35]) cube([6,5.7,50],center=true);
            			}
			}
			// extra 0.7mm hole width to compensate for deviations
			translate([-filament_feed_hole_offset+25.7*((mount<1)?1:-1),
				-wade_tractor_translation[1]-1,wade_block_depth/2])
				rotate([-90,0,0]) rotate(360/16)
					cylinder(r=m4_diameter/2,h=base_thickness+2,$fn=12);
              	}
		}
	}

	for (a=[-1,1]){
	  translate([0,
			     idler_mounting_hole_up+wade_tractor_translation[1],
			     wade_block_depth/2+idler_mounting_hole_across*a])

	  rotate([0,90,0]){
		// nut slots
		translate([0,10/2,wade_block_width-idler_nut_trap_depth+5.2/2])
		  cube([idler_nut_wrench+.3,10,5.2],center=true);

		// screw holes 30°
		for(b=[0:6]){
		  translate([0,0,(wade_block_width-idler_nut_trap_depth+idler_nut_thickness/2)])
		    rotate([b*5,0,0])
		    rotate([0,0,30]) 
			translate([0,0,-19.5])
			  cylinder(r=idler_mounting_hole_diameter/2,h=wade_block_depth+5.5,$fn=6);
		}
		// nut traps 30°
		for(c=[0:6]) {
		  translate([0,0,wade_block_width-idler_nut_trap_depth+5.2/2])
			rotate([c*5,0,0])
			rotate([0,0,30])
			nut_trap(idler_nut_wrench,5.2);
		}
	  }
	}

	// open an access to de back of the idler mouting holes [v301]
	translate([wade_block_width+1.6,wade_block_height-12,
        wade_block_depth/2-idler_mounting_hole_across-((idler_mounting_hole_diameter*cos(180/6))/2)])
        cube([3,8.5,7]);

	 translate([wade_block_width/2,37,wade_block_depth/2-8/2])
			rotate([0,0,-147])
			cube([3.5,30,8]);

	translate([wade_block_width+5, 5+base_thickness,-0.1]) 
		cylinder(r=5,h=motor_mount_thickness+0.2);
	translate([wade_block_width+18,2+base_thickness,-0.1])
		cylinder(r=5,h=motor_mount_thickness+0.2);
	translate([wade_block_width+7,wade_tractor_translation[1],-.1])
		cylinder(h=motor_mount_thickness+0.2,r=5);
	translate([wade_block_width+7,40+base_thickness,-.1])
		cylinder(h=motor_mount_thickness+0.2,r=5);  	// [v304]
}

module motor_mount_holes(){
		translate([0,0,-0.1])
			for (hole=[1:3]){
				hull() {
					translate([motor_hole(hole)[0]-motor_slot_left,motor_hole(hole)[1],0])
						cylinder(h=motor_mount_thickness+.2,r=m3_diameter/2);
					translate([motor_hole(hole)[0]+motor_slot_right,motor_hole(hole)[1],0])
						cylinder(h=motor_mount_thickness+.2,r=m3_diameter/2);
				}		
		 		hull(){
					translate([motor_hole(hole)[0]-motor_slot_left,motor_hole(hole)[1],0])
						cylinder(h=screw_head_recess_depth,r=screw_head_recess_diameter/2);
					translate([motor_hole(hole)[0]+motor_slot_right,motor_hole(hole)[1],0])
						cylinder(h=screw_head_recess_depth,r=screw_head_recess_diameter/2);
				}
			}
        	hull() {
        		translate(motor_mount_translation+[-motor_slot_left+0.5,0,-0.1])
        			cylinder(r=motor_center_hole/2,h=wade_block_depth);
        		translate(motor_mount_translation+[motor_slot_right,0,-0.1])
        			cylinder(r=motor_center_hole/2,h=wade_block_depth);
        	}
		hull(){
			translate([motor_hole(1)[0]-motor_slot_left,motor_hole(1)[1],motor_mount_thickness])
				cylinder(r=nema_support_d/2+1,h=wade_block_depth);
			translate([motor_hole(2)[0]-motor_slot_left,motor_hole(2)[1],motor_mount_thickness])
				cylinder(r=nema_support_d/2+1,h=wade_block_depth);
			translate([motor_hole(3)[0]-motor_slot_left,motor_hole(3)[1],motor_mount_thickness])
				cylinder(r=nema_support_d/2+1,h=wade_block_depth);		//[v304]
		}
}

module wadeidler(){
	guide_height=12.3;
	guide_length=8.8;

	difference(){
		union(){
			//The idler block.
			translate(idler_axis+[-idler_height/2+2,idler_long_side/2-idler_long_bottom,0]){
				cube([idler_height,idler_long_side,idler_short_side],center=true);

				//Filament Guide, only the moving half
				translate([idler_height/2-0.1,idler_long_side/2-guide_length,-5.6])
					// Reinforcement
					difference(){
						translate([-0.1,-5.5,-4.8]) cube([1.6,7,20.8]);
						translate([-0.1,-5.5,-4.9]) rotate([0,0,-45]) cube([2,3,22]);
						translate([2.1,-1.6,-4.9]) rotate([0,0,45]) cube([2,43,22]);
						translate([-0.1,-5.5,-4.9]) rotate([0,45,0]) cube([2,10,3]);
						translate([-0.1,-5.5,-4.9+20.8]) rotate([0,45,0]) cube([3,10,2]);
						translate([-.1,-5.4,+1.1]) cube([1.7,7,9]);
					}
			}
			// The fulcrum Hinge
			translate(idler_fulcrum){
				rotate([0,0,-30]){
					cylinder(h=idler_short_side,r=idler_hinge_rh+0.5,center=true,$fn=60);
					translate([-idler_end_length/2,0,0])
					cube([idler_end_length,(idler_hinge_rh+0.5)*2,idler_short_side],center=true);
				}
			}
		}

		//Back of idler
		translate(idler_axis+[-idler_height/2+2-idler_height,
			idler_long_side/2-idler_long_bottom,0])
		cube([idler_height,idler_long_side+13,idler_short_side+2],center=true);

		//Slot for idler fulcrum mount
		translate(idler_fulcrum){
			cylinder(h=idler_short_side-2*idler_hinge_width,
				r=idler_hinge_rh+0.6,center=true,$fn=60);
			rotate(-32)
			translate([0,-idler_hinge_rh-0.5,0])
			cube([idler_hinge_rh*2+1,idler_hinge_rh*2+1,
				idler_short_side-2*idler_hinge_width],center=true);

		}

		//Bearing cutout. Kugelleager Aufhaengung
		translate(idler_axis){
			difference(){
				cylinder(h=idler_608_height,r=idler_608_diameter/2,
					center=true,$fn=60);
				for (i=[0,1])
				rotate([180*i,0,0])
				translate([0,0,6.9/2])
				cylinder(r1=13/2,r2=17/2,h=2);
			}
			cylinder(h=idler_short_side-6,r=m8_diameter/2-0.25, center=true);
			translate([2,0,-idler_608_height/2])
				cube([5,idler_608_diameter/2,idler_608_height]);
		}

		//Fulcrum hole. Gelenkloch
		translate(idler_fulcrum)
		rotate(360/12)
		cylinder(h=idler_short_side+2,r=m3_diameter/2-0.2,center=true,$fn=12);

		//Nut trap for fulcrum screw. Loch fuer Mutter
		translate(idler_fulcrum+[0,0,idler_short_side/2-idler_hinge_width+1])
		nut_trap(m3_wrench,3);

		for(idler_screw_hole=[-1,1])
		  translate(idler_axis+[2-idler_height,0,0]){
			//Screw Holes. Schraubenloecher
			translate([-1,idler_mounting_hole_up-1,
				idler_screw_hole*idler_mounting_hole_across])
			rotate([0,90,0]){
			  translate([0,-idler_mounting_hole_diameter/2-0.7,0]) rotate([0,0,45])
			    cube([idler_mounting_hole_diameter*0.707,idler_mounting_hole_diameter*0.707,idler_height+5]);
				translate([-idler_mounting_hole_diameter/2,-0.7,0])
				  cube([idler_mounting_hole_diameter,idler_mounting_hole_elongation+7.7, idler_height+2]);
			}
		}
		
		// Cavity por the Quick Release Lever
		translate(idler_axis+[-idler_height/2+2,+idler_long_side/2-idler_long_bottom,0])
		translate([-idler_height/2-1.5,idler_long_side/2-13,-5])
		rotate([0,0,0-7.2])
			cube([1.5,12,10]);
	}
		translate(idler_axis+[-idler_height/2+2,+idler_long_side/2-idler_long_bottom,-3])
		for (a=[0:3]){
			difference(){		
			translate([-idler_height/2,idler_long_side/2-13,2*a])
				cube([1.5,12,.2]);
			translate([-idler_height/2,idler_long_side/2-13,-2])
		rotate([0,0,0-7.2])
			cube([1.5,12,10]);
			}
		}
}

module lever(){
	difference(){
		union(){
			translate([2,0,0]) cylinder(r=17/2,h=lever_width);
			rotate([0,0,-90]) cube([8.5,10,lever_width]);
			translate([-1,0,0]) rotate([0,0,-90]) cube([28,10,lever_width]);
			translate([-1,-16.6,0]) rotate([0,0,20]) cube([4,15,lever_width]);
			translate([-1,-14,0]) rotate([0,0,-101.6])  cube([17,5,lever_width]);
		}
		translate([-0.3,0,-0.1]) cylinder(r=lever_hole/2, h=lever_width+0.2);
		translate([10,2.9,-0.1]) rotate([0,0,-90-lever_inclination]) cube([13,2,lever_width+0.2]);
		translate([18.5,-14,-0.1]) cylinder(r=24/2, h=lever_width+0.2);
		translate([6.75,-36,-0.1]) cylinder(r=21/2, h=lever_width+0.2);// finger space
  	}
	translate([-3,-30.6,0]) cylinder(r=1.4, h=lever_width, $fn=12);
}

module bar(){
	difference(){
		translate([-bar_radiusr+2,0,0]) cylinder(h=bar_length,r=bar_radiusr);
		translate([0,-bar_radiusr,-.1]) cube([bar_diameter,bar_diameter,bar_length+.2]);
		translate([0,0,5]) rotate([0,90,180]) m4Screw();
		translate([0,0,bar_length-5])  rotate([0,90,180])  m4Screw();
	}
}

module m4Screw(h=15) {
	union() {
		translate([0,0,3+layer_thickness]) cylinder(h=5,r=4.3/2);
		translate([0,0,-0.1]) cylinder(h=3,r=bar_spring/2,$fn=18);
	}
}

module b608(height=7){
	translate([0,0,height/2]) cylinder(r=hole_for_608/2,h=height,center=true,$fn=100);
}

module b688(height=5){
	translate([0,0,height/2]) cylinder(r=hole_for_688zz/2,h=height,center=true,$fn=100);
}

//========================================================
// Modules for defining holes for hotend mounts:
// These assume the extruder is verical with the bottom filament exit hole at [0,0,0].

//malcolm_hotend_holes ();
module malcolm_hotend_holes (){
	extruder_recess_d=16.7;
	extruder_recess_h=3.7;

	// Recess in base
	translate([0,0,-1])
	cylinder(r=extruder_recess_d/2,h=extruder_recess_h+1);
}

//groovemount_holes ();
module groovemount_holes (){
	extruder_recess_d=16.7;
	extruder_recess_h=4.6;

	// Recess in base
	translate([0,0,-1])
	cylinder(r=extruder_recess_d/2,h=extruder_recess_h+1);
}

//peek_reprapsource_holes ();
module peek_reprapsource_holes (){
	extruder_recess_d=11.5;
	extruder_recess_h=19;

	// Recess in base
	translate([0,0,-1])
	cylinder(r=extruder_recess_d/2,h=extruder_recess_h+1);

	// Mounting holes to affix the extruder into the recess.
	translate([0,0,min(extruder_recess_h/2, base_thickness)])
	rotate([-90,0,0])
	cylinder(r=m4_diameter/2-0.5, h=wade_block_depth+2,center=true);
}

//arcol_mount_holes();
module arcol_mount_holes(){
	hole_axis_rotation=42.5;
	hole_separation=30;
	hole_slot_height=4;
	for(mount=[-1,1])
		translate([hole_separation/2*mount,-7,0]) {
			translate([0,0,-1])
				cylinder(r=m4_diameter/2,h=base_thickness+2,$fn=8);

				translate([0,0,base_thickness/2])
				//rotate(hole_axis_rotation){
					cylinder(r=m4_nut_diameter/2,h=base_thickness/2+hole_slot_height,$fn=6);
				translate([0,-m4_nut_diameter,hole_slot_height/2+base_thickness/2])
					cube([m4_nut_diameter,m4_nut_diameter*2,hole_slot_height],center=true);
		}
}

//mendel_parts_v6_holes ();
module mendel_parts_v6_holes (insulator_d=12.7) {
	extruder_recess_d=insulator_d+0.7;
	extruder_recess_h=10;
	hole_axis_rotation=42.5;
	hole_separation=30;
	hole_slot_height=5;

	// Recess in base
	translate([0,0,-1])
	cylinder(r=extruder_recess_d/2,h=extruder_recess_h+1);

	for(mount=[-1,1])
	rotate([0,0,hole_axis_rotation+90+90*mount])
	translate([hole_separation/2,0,0]){
		translate([0,0,-1])
		cylinder(r=m4_diameter/2,h=base_thickness+2,$fn=8);

		translate([0,0,base_thickness/2])
		rotate(-hole_axis_rotation+180){
//			rotate(30)
			cylinder(r=m4_nut_diameter/2,h=base_thickness/2+hole_slot_height,$fn=6);
			translate([0,-m4_nut_diameter,hole_slot_height/2+base_thickness/2])
			cube([m4_nut_diameter,m4_nut_diameter*2,hole_slot_height],
					center=true);
		}
	}
}

//grrf_peek_mount_holes();
module grrf_peek_mount_holes(){
	extruder_recess_d=16.5;
	extruder_recess_h=10;

	// Recess in base
	translate([0,0,-1])
	cylinder(r=extruder_recess_d/2,h=extruder_recess_h+1);

	for (hole=[-1,1]){		
		rotate(90,[1,0,0])
			translate([hole*(extruder_recess_d/2-1.5),3+1.5,-wade_block_depth/2-1]){
				translate([0,0,3.25+layer_thickness])
					cylinder(r=1.5,h=wade_block_depth+2,$fn=10);
				translate([0,0,1.75])
					nut_trap(m3_wrench, 3);
			}
	}
}

//wildseyed_mount_holes(insulator_d=16); //  16 is standard (J-head)
module wildseyed_mount_holes(insulator_d=12.7,wildseyed_fix=false){
	extruder_recess_d=insulator_d+0.4;	// from 0.7 to 0.4
	extruder_recess_h=10+0.8;			// a little extra...

	// Recess in base
	translate([0,0,-1])
	cylinder(r=extruder_recess_d/2,h=extruder_recess_h+1,$fn=36);
	if ([wildseyed_fix]){
		for (hole=[-1,1])
		rotate(90,[1,0,0])
		translate([hole*(extruder_recess_d/2-1.5),3+1.5,-wade_block_depth/2-1])
		cylinder(r=(3-0.1)/2,h=wade_block_depth+2,$fn=16);// adjusted diameter
	}
}
//p3steel_mount_holes(insulator_d=16); //  16 is standard (J-head/buda/...)
module p3steel_mount_holes(insulator_d=16,wildseyed_fix=false){
	extruder_recess_d=insulator_d+0.5;	// from 0.7 to 0.4
	extruder_recess_h=10;
	hole_separation=33;
	union(){
		translate([0,0,-.1])
			cylinder(r=extruder_recess_d/2,h=extruder_recess_h+.1);
		if (wildseyed_fix){
			for (x=[0:5]){
				union(){
					rotate([0,0,60*x+30])
						translate([hole_separation/2,0,-.2]) cylinder(r=m3_diameter/2,h=10.2);
					rotate([0,0,60*x+30])
						translate([hole_separation/2,0,-.2]) cylinder(r=6.4/2,h=3.8+.2);
					rotate([0,0,60*x+30])
						translate([hole_separation/2,0,3.8]) cylinder(r1=6.4/2,r2=m3_diameter/2,h=1);
				}
			}
		}
	}
}
// conical screw head for Jhead 2.8x25mm (BR)
module jh_conical_screw(){
	extruder_recess_d=16+0.7; // for j-head only
	for (hole=[-1,1])
	rotate(90,[1,0,0])
	translate([hole*(extruder_recess_d/2-1.5),3+1.5,wade_block_depth/2-1.7])
	 cylinder(r1=1.5,r2=2.76,h=1.71,$fn=12);
}

//PEEK mount holes for reprap-fab.org 10mm dia insulator
module peek_reprapfaborg_holes(){
	extruder_recess_d=10.8;
	extruder_recess_h=20;

	// Recess in base
	translate([0,0,-1])
	cylinder(r=extruder_recess_d/2,h=extruder_recess_h+1);

	// Mounting holes to affix the extruder into the recess.
	translate([5,0,min(extruder_recess_h/2, base_thickness-2)])
	rotate([-90,0,0]){
	polyhole(2.5,wade_block_depth+2);}
	translate([-5,0,min(extruder_recess_h/2, base_thickness-2)])
	rotate([-90,0,0]){
	polyhole(2.5,wade_block_depth+2);}
}

module nut_trap(nut_wrench_size,trap_height,vertical=true, clearance=0.2){		// M3 wrench size = 5.5
	cornerdiameter =  (((nut_wrench_size)/2)+clearance) / cos(180/6);
	rotate([0,vertical*90,0])
		cylinder(h = trap_height, r = cornerdiameter, center=true, $fn = 6);
}

module nut_slot(nut_wrench_size,nut_height, nut_elevation,vertical=false, clearance=0.2){
	// M3 wrench size = 5.5
	cornerdiameter =  (((nut_wrench_size)/2)+clearance) / cos(180/6);
	slot_height = nut_height+2*clearance;
	slot_width = nut_wrench_size+2*clearance;
	rotate([0,vertical*270,0]){
		cylinder(h = slot_height, r = cornerdiameter, center=true, $fn = 6);
		translate([0,-slot_width/2,-slot_height/2]) cube([nut_elevation+1,slot_width,slot_height]);
	}
}

module universel(p3_steel_universel) {
	ring_out=16.4;
	difference (){
		cylinder(r=16.2/2,h=p3_steel_universel);
		cylinder(r=filament_feed_hole_d/2,h=p3_steel_universel);
		translate([0,0,-.1])
			cylinder(r1=filament_feed_hole_d/2+.5,r2=filament_feed_hole_d/2,h=1);
		translate([0,0,p3_steel_universel-1+.1])
			cylinder(r2=filament_feed_hole_d/2+.5,r1=filament_feed_hole_d/2,h=1);
		translate([0,0,p3_steel_universel-1+.1])
		difference () {
			cylinder(r=18/2,h=1,$fn=32);
			cylinder(r1=ring_out/2,r2=ring_out/2-1,h=1,$fn=32);
		}
		translate([0,0,-.1]) difference () {
			cylinder(r=18/2,h=1,$fn=32);
			cylinder(r2=ring_out/2,r1=ring_out/2-1,h=1,$fn=32);
		}
	}
}

module p3steel_nozzle_e3d(){
	ring_in=12.2;
	ring_out=16.4;
	ring_depth=3;
	universel(10-2.7-3);
	translate([0,20,0])
		difference (){
			cylinder(r=ring_out/2,h=6);
			translate([0,0,-.1]) cylinder(r=ring_in/2,h=6+.2);
			translate([3,0,4.5]) cube ([12,ring_out,ring_depth+.2],center=true);
			translate([0,ring_out/4+6,4.5]) cube ([ring_out,ring_out/2,ring_depth+.2],center=true);
			translate([0,-ring_out/4-6,4.5]) cube ([ring_out,ring_out/2,ring_depth+.2],center=true);


		}
}

module p3steel_nozzle_buda(){
	ring_1=12.2;
	ring_2=16.4;
	ring_depth1=3;
	ring_depth2=10;
	difference (){
		union(){
			cylinder(r=ring_2/2,h=ring_depth2);
			translate([0,0,ring_depth2]) cylinder(r=ring_1/2,h=ring_depth1);
		}
		cylinder(r=filament_feed_hole_d/2,h=ring_depth1+ring_depth2);
		translate([0,0,-.1])
			cylinder(r1=filament_feed_hole_d/2+.5,r2=filament_feed_hole_d/2,h=1);
		translate([0,0,ring_depth1+ring_depth2-1+.1])
			cylinder(r2=filament_feed_hole_d/2+.5,r1=filament_feed_hole_d/2,h=1);

		translate([0,0,ring_depth2]) cylinder(r=6.4/2,h=ring_depth1+.1);
		translate([0,0,-.1]) difference () {
			cylinder(r=18/2,h=1,$fn=32);
			cylinder(r2=ring_2/2,r1=ring_2/2-1,h=1,$fn=32);
		}
	}
}


module stepperMotor(caseSize, caseHeight, holeSpacing, holeDiameter, shaftDiameter, shaftHeight, shaftCollarDiameter, shaftCollarThickness) {
	union() {
		translate([0,0,0])  cylinder(r=shaftDiameter/2, h=shaftHeight+1);	
		translate([0,0,0])  cylinder(r=shaftCollarDiameter/2, h=shaftCollarThickness);

		difference(){
			translate([-caseSize/2,-caseSize/2,-caseHeight])  cube([caseSize, caseSize, caseHeight]);
			translate([ holeSpacing/2,  holeSpacing/2,-5])  cylinder(r=holeDiameter/2, h=5.1);
			translate([ holeSpacing/2, -holeSpacing/2,-5])  cylinder(r=holeDiameter/2, h=5.1);
			translate([-holeSpacing/2, holeSpacing/2,-5])  cylinder(r=holeDiameter/2, h=5.1);
			translate([-holeSpacing/2,-holeSpacing/2,-5])  cylinder(r=holeDiameter/2, h=5.1);
		}
	}
	
}

module nozzle_e3d() {
	color("silver"){
		difference(){
		//top mounting point
			union(){
				translate([0,0,8.64]) cylinder(3.66,8,8);
				cylinder(12.3,6,6);
				cylinder(3.04,8,8);
				//small thermal break
				translate([0,0,-6.6]) cylinder(6.6,4.575,4.575);
				translate([0,0,-4.15]) cylinder(1.9,8,8);
				//big thermal break
				translate([0,0,-38.14]) cylinder(31.54,4.575,4.575); 	//smallest inner shaft
				translate([0,0,-38.14]) cylinder(18.88,6.5,6.5); 		//medium inner shaft
				translate([0,0,-38.14]) cylinder(14.66,7.5,7.5); 		//large-bottom inner shaft
				for (z = [0:7]){
					translate([0,0,-8.8-4.2*z]) cylinder(2,12.5,12.5);
				}
			} 
			//heatsink fins
			//through and through hole
			union(){
				translate([0,0,10.31]) cylinder(2,1,4.465); //filament funnel - from ref. drawing
				translate([0,0,-39.1]) cylinder(51.4,1,1);
			}
		}
	} 
	//filament hole
	
	//begin heater block (from ref. drawings and extrapolation, not measured)
	color("gray"){
		translate([0,0,-40.14]) cylinder(2.1,1.2,1.2);
		difference(){
			translate([-4,-7.5,-48.14]) cube([15,15,8]);
			union(){
				translate([7,8,-44.14]) rotate([90,0,0]) cylinder(16,2,2,$fn=20);
				translate([9,-4.5,-46.9]) rotate([90,0,0]) cylinder(3.1,.5,.5,$fn=20);
			}
		}
	}
	translate([0,0,-51.14]) cylinder(3,4,4,$fn=6);
	color("goldenrod") translate([0,0,-53.14]) cylinder(2,.4,3.5,$fn=20);
}